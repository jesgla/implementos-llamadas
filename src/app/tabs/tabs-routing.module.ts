import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'clienteTab/:actualizar',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./cliente-tabs/cliente-tabs.module').then(m => m.ClienteTabsPageModule)
          }
        ]
      },
      {
        path: 'contactados-tabs',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./contactados-tabs/contactados-tabs.module').then(m => m.ContactadosTabsPageModule)
          }
        ]
      },
      {
        path: 'historial-tabs',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./historial-tabs/historial-tabs.module').then(m => m.HistorialTabsPageModule)
          }
        ]
      },
      {
        path: 'tab3',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tab3/tab3.module').then(m => m.Tab3PageModule)
          }
        ]
      },
     
      {
        path: '',
        redirectTo: '/tabs/clienteTab/:actualizar',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/clienteTab/:actualizar',
    pathMatch: 'full'
  },
  /* { path: 'historial-tabs', loadChildren: './historial-tabs/historial-tabs.module#HistorialTabsPageModule' }, */
 /*  { path: 'contactados-tabs', loadChildren: './contactados-tabs/contactados-tabs.module#ContactadosTabsPageModule' }, */
/*   { path: 'cliente-tabs', loadChildren: './cliente-tabs/cliente-tabs.module#ClienteTabsPageModule' } */
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
