import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactadosTabsPage } from './contactados-tabs.page';

describe('ContactadosTabsPage', () => {
  let component: ContactadosTabsPage;
  let fixture: ComponentFixture<ContactadosTabsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactadosTabsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactadosTabsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
