import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ContactadosTabsPage } from './contactados-tabs.page';
import { ContactadosPageModule } from 'src/app/pages/page/contactados/contactados.module';

const routes: Routes = [
  {
    path: '',
    component: ContactadosTabsPage
  }
];

@NgModule({
  imports: [
    ContactadosPageModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ContactadosTabsPage]
})
export class ContactadosTabsPageModule {}
