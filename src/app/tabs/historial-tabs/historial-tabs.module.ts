import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HistorialTabsPage } from './historial-tabs.page';
import { HistorialLlamadaPageModule } from 'src/app/pages/page/historial-llamada/historial-llamada.module';

const routes: Routes = [
  {
    path: '',
    component: HistorialTabsPage
  }
];

@NgModule({
  imports: [
    HistorialLlamadaPageModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HistorialTabsPage]
})
export class HistorialTabsPageModule {}
