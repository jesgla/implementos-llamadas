import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClienteTabsPage } from './cliente-tabs.page';

describe('ClienteTabsPage', () => {
  let component: ClienteTabsPage;
  let fixture: ComponentFixture<ClienteTabsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClienteTabsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClienteTabsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
