import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ClienteTabsPage } from './cliente-tabs.page';
import { ClientesPageModule } from 'src/app/pages/page/clientes/clientes.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ClientesPageModule,
    RouterModule.forChild([{ path: '', component: ClienteTabsPage }])
  ],
  declarations: [ClienteTabsPage]
})
export class ClienteTabsPageModule {}
