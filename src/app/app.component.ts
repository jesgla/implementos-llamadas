import { Component } from '@angular/core';

import { Platform, Events } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AutentificacionService } from './services/autentificacion/autentificacion.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    public events: Events,
    
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private autentificacionService: AutentificacionService,
    private router: Router
  ) {
    this.events.publish('actualizar',true);
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleLightContent();
      this.splashScreen.hide();

      this.autentificacionService.authenticationState.subscribe(state => {
        if (state) {
          this.router.navigate([`tabs/clienteTab/`,'iniciar']);
          //this.router.navigate(['tabs', 'clienteTab'], { queryParams: {actualizar: 'folder'} });
        } else {
          this.router.navigate(['login']);
        }
      });

    });
  }
}
