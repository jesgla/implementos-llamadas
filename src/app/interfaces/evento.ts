/**
 *
 *
 * @export
 * @ignore
 * @class Evento
 */
export class Evento{
    idEvento: string;
    referenciaEvento: string;
    rutEmisor: string;
    rutVendedor: string;
    rutCliente: string;
    nombreCliente: string;
    tipoEvento: string;
    fechaCreacion: string;
    fechaEjecucion: string;
    fechaProgramada: string;
    observaciones: string;
    estado: string;
    direccion: string;
    latitud: string;
    longitud: string;
    refVisita: string;
    detalle : string;
  }