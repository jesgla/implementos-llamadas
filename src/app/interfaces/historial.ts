export class Historial {
    totalLlamadas: number;
    llamadasContestadas: any[];
    llamadasNoContestadas: any[];
    porcentajeContestadas: number;
    porcentajeNoContestadas: number;
    sumaNoContestadas: number;
  sumaContestadas: number;

}