import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtros'
})
export class FiltrosPipe implements PipeTransform {

  transform(arreglo: any[],
    texto: string,
    columna: string,
    columna2:string): any[] {

    if (texto.trim().length === 0) {
      return arreglo;
    }
    console.log(texto)
    texto = texto.toLowerCase();


    return arreglo.filter(item => {
      return item[columna].toLowerCase()
        .includes(texto)||
          item[columna2].includes(texto)
    });

  }

}
