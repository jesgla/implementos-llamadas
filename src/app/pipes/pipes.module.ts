import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FiltrosPipe } from './filtros.pipe';
import { FormatoMonedaPipe } from './formato-moneda.pipe';



@NgModule({
  declarations: [FiltrosPipe, FormatoMonedaPipe],
  imports: [
    CommonModule
  ],
  exports:[FiltrosPipe,FormatoMonedaPipe]
})
export class PipesModule { }
