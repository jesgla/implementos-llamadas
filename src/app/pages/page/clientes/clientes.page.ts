import { Component, OnInit } from '@angular/core';
import { AutentificacionService } from 'src/app/services/autentificacion/autentificacion.service';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { CallLog } from '@ionic-native/call-log/ngx';
import { Platform, Events } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.page.html',
  styleUrls: ['./clientes.page.scss'],
})
export class ClientesPage{
  vendedor: any;
  lstClientes: any;
  listTyle: string;
  filters: { name: any; value: any; operator: any; }[];
  recordsFoundText: string;
  recordsFound: any;
  textoBuscar :string = '';

  constructor(
    public events: Events,
    private activeRoute:ActivatedRoute,
    private authService: AutentificacionService, 
    private clienteService : ClienteService,
    private callLog: CallLog,
    private platform: Platform) { 
    this.lstClientes=[];
    this.recordsFound=[];
    this.events.subscribe('actualizar', (data) =>{
      console.log('data',data); // 👋 Hello from page1!
     this.ionViewWillEnter()
    });
    this.platform.ready().then(() => {
 
      this.callLog.hasReadPermission().then(hasPermission => {
        if (!hasPermission) {
          this.callLog.requestReadPermission().then(results => {
            
          })
            .catch(e => console.log(" requestReadPermission " + JSON.stringify(e)));
        } else {
          this.getContacts("type","2","==")
        }
      })
        .catch(e => console.log(" hasReadPermission " + JSON.stringify(e)));
    });
 
  }
  

  async ionViewWillEnter() {
   
    let argumento = this.activeRoute.snapshot.paramMap.get('actualizar');
   
    this.lstClientes = await this.clienteService.obtenerClientes();
    console.log('this.lstClientes',this.lstClientes)
    this.lstClientes.filter(cli=>console.log(cli.rut==='12887820-3'))
    await this.getContacts("type","2","==");
    await this.registrarLlamadas();
    this.lstClientes = this.lstClientes.filter(cliente=>cliente.registros.length==0);
  }
 
  registrarLlamadas() {
    for (let index = 0; index < this.lstClientes.length; index++) {
      const cliente = this.lstClientes[index];
      for (let indice = 0; indice < this.recordsFound.length; indice++) {
        const llamada = this.recordsFound[indice];
        const existe = cliente.registros.find( registro => registro.idllamada === llamada.date.toString() );

        if(cliente.celular ===llamada.number /* && llamada.duration>10 */ && !existe){
          let objeto ={
            rut:cliente.rut,
            celular:cliente.celular,
            segundos : llamada.duration,
            idllamada: llamada.date.toString()
          };
          this.clienteService.guardarLlamada(objeto);

        }
      }
    }
  }
  logout() {
    this.authService.logout();
  }
  getContacts(name, value, operator) {
    if(value == '1'){
      this.listTyle = "Incoming Calls from yesterday";
    }else if(value == '2'){
      this.listTyle = "Ougoing Calls from yesterday";
    }else if(value == '5'){
      this.listTyle = "Rejected Calls from yesterday";
    }

    //Getting Yesterday Time
    var today = new Date();
    var yesterday = new Date(today);
    yesterday.setDate(today.getDate() - 1);
    var fromTime = yesterday.getTime();

    this.filters = [{
      name: name,
      value: value,
      operator: operator,
    },{
      name: "date",
      value: fromTime.toString(),
      operator: ">=",
    } ];
    this.callLog.getCallLog(this.filters)
      .then(results => {
        this.recordsFoundText = JSON.stringify(results);
        
        this.recordsFound = results;//JSON.stringify(results);
      })
      .catch(e => console.log(" LOG " + JSON.stringify(e)));
  }

  async doRefresh(event) {
    console.log('Begin async operation');
    this.lstClientes = await this.clienteService.obtenerClientes();
    await this.getContacts("type","2","==");
    await this.registrarLlamadas();
    this.lstClientes = this.lstClientes.filter(cliente=>cliente.registros.length==0);

    event.target.complete();
/* 
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000); */
  }

  buscar( event ) {
    this.textoBuscar = event.detail.value;
  }
}
