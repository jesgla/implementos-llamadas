import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactadosPage } from './contactados.page';

describe('ContactadosPage', () => {
  let component: ContactadosPage;
  let fixture: ComponentFixture<ContactadosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactadosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactadosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
