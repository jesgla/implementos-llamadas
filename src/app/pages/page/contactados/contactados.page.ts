import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { Events } from '@ionic/angular';

@Component({
  selector: 'app-contactados',
  templateUrl: './contactados.page.html',
  styleUrls: ['./contactados.page.scss'],
})
export class ContactadosPage implements OnInit {
  textoBuscar :string = '';
  lstClientes: any[];

  constructor(
    private clienteService : ClienteService,
    public events: Events,
    ) {
    this.lstClientes=[];
    this.events.subscribe('actualizar', (data) =>{
     this.ionViewWillEnter()
    });
   }
  

  ngOnInit() {
  }
  async ionViewWillEnter() {
    this.lstClientes = await this.clienteService.obtenerClientes();

    this.lstClientes = this.lstClientes.filter(cliente=>cliente.registros.length>0);

    console.log('clientes',this.lstClientes )
  }
 
  async doRefresh(event) {
    console.log('Begin async operation');
    this.lstClientes = await this.clienteService.obtenerClientes();
    this.lstClientes = this.lstClientes.filter(cliente=>cliente.registros.length>0);
 
    event.target.complete();
/* 
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000); */
  }

  buscar( event ) {
    this.textoBuscar = event.detail.value;
  }
}
