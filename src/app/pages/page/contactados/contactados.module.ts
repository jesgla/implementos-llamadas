import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ContactadosPage } from './contactados.page';
import { ClienteComponentsModule } from '../../components/cliente-components.module';

const routes: Routes = [
  {
    path: '',
    component: ContactadosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ClienteComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ContactadosPage]
})
export class ContactadosPageModule {}
