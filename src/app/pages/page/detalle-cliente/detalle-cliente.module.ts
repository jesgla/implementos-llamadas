import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetalleClientePage } from './detalle-cliente.page';
import { ComentarioLlamadaComponent } from '../../components/modals/comentario-llamada/comentario-llamada.component';
import { ClienteComponentsModule } from '../../components/cliente-components.module';

const routes: Routes = [
  {
    path: '',
    component: DetalleClientePage
  }
];

@NgModule({
  entryComponents:[ComentarioLlamadaComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ClienteComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetalleClientePage]
})
export class DetalleClientePageModule {}
