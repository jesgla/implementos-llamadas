import { Component, OnInit, AfterViewInit, ViewChild, Input } from '@angular/core';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { IonSlides, ModalController, NavController, Events } from '@ionic/angular';
import { ComentarioLlamadaComponent } from '../../components/modals/comentario-llamada/comentario-llamada.component';

@Component({
  selector: 'app-detalle-cliente',
  templateUrl: './detalle-cliente.page.html',
  styleUrls: ['./detalle-cliente.page.scss'],
})
export class DetalleClientePage implements OnInit, AfterViewInit {
  cliente: any;
  datos: any;
  llamadas:any[];
  /**
   *elemento slider
   * @type {IonSlides}
   * @memberof DetalleClientePage
   */
  @ViewChild(IonSlides, { static: false }) theSlides: IonSlides;

  /**
   *color de elemento
   * @type {*}
   * @memberof DetalleClientePage
   */
  color: any;

  /**
   *arreglo de paginas de slider
   * @memberof DetalleClientePage
   */
  paginas = [
    { icon: 'person', index: 0 },
    { icon: 'clipboard', index: 1 },
    { icon: 'document', index: 2 },
    { icon: 'text', index: 3 },
    { icon: 'call', index: 4 },
    { icon: 'folder', index: 5 },
  ];
  listadoArticulo: [];
   constructor(
    public events: Events,
    public modalController: ModalController,
    public navCtrl: NavController,
    private dataLocal: DataLocalService,
    private clienteService: ClienteService
  ) {
    console.log(this.events);
 
    this.cliente = {};

    this.listadoArticulo = [];

  }

  async ngOnInit() {
    this.llamadas=[];
    this.cliente = await this.dataLocal.getItem('cliente');

    this.datos = await this.clienteService.obtenerCliente(this.cliente);
    this.cliente.correo = this.datos.correos && this.datos.correos[0] ? this.datos.correos[0].correo : 'no registrado';

    this.listadoArticulo = await this.clienteService.obtenerMasVendidos(this.cliente);

    let registros = this.cliente.registros;
    console.log('registros',registros)
    registros.map(llamada => {
      let llamadaCliente = {
        rut: this.cliente.rut,
        nombre: this.cliente.nombre,
        fecha: llamada.idllamada,
        segundos: llamada.segundos
      }

      this.llamadas.push(llamadaCliente);
    })

    console.log('this.llamadas',this.llamadas)
  }

  /**
    *metodo que se ejecuta despues de iniciar la vista
    * @memberof DetalleClientePage
    */
  ngAfterViewInit() {

    this.theSlides.lockSwipes(true);

  }

  /**
   *permite cambiar la vista de slider
   * @param {*} event
   * @param {*} slide
   * @memberof DetalleClientePage
   */
  cambioPagina(event, slide) {

    this.theSlides.lockSwipes(false);

    this.theSlides.slideTo(event.detail.value.index);
    this.theSlides.lockSwipes(true);

  }
  async abrirModal() {

    const modal = await this.modalController.create({
      component: ComentarioLlamadaComponent,
      componentProps: {
        cliente: this.cliente
      }
    });
    return await modal.present();

  }
}
