import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistorialLlamadaPage } from './historial-llamada.page';

describe('HistorialLlamadaPage', () => {
  let component: HistorialLlamadaPage;
  let fixture: ComponentFixture<HistorialLlamadaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistorialLlamadaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistorialLlamadaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
