import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HistorialLlamadaPage } from './historial-llamada.page';
import { ClienteComponentsModule } from '../../components/cliente-components.module';

const routes: Routes = [
  {
    path: '',
    component: HistorialLlamadaPage
  }
];

@NgModule({
  imports: [
    ClienteComponentsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HistorialLlamadaPage]
})
export class HistorialLlamadaPageModule {}
