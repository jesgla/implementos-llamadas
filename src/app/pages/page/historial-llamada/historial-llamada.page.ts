import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { Historial } from 'src/app/interfaces/historial';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-historial-llamada',
  templateUrl: './historial-llamada.page.html',
  styleUrls: ['./historial-llamada.page.scss'],
})
export class HistorialLlamadaPage implements AfterViewInit {
  @ViewChild(IonSlides, { static: false }) slideLlamada: IonSlides;
  lstClientes: any[];
  historial: Historial;
  paginas = [
    { titulo: 'Contestadas', index: 0 },
    { titulo: 'No contestadas', index: 1 },
  
  ];
  constructor(private clienteService: ClienteService) {
    this.historial = new Historial();
  }

  async ionViewWillEnter() {
    this.lstClientes = await this.clienteService.obtenerClientes();
    console.log('this.lstClientes', this.lstClientes);
    this.formatearDatos();
  }
  formatearDatos() {
    let sumaLlamadas = 0;
    let sumaNoContestadas = 0;
    let sumaContestadas = 0;
    let llamadasContestadas = [];
    let llamadasNoContestadas = [];
    this.lstClientes.map(cliente => {
      sumaLlamadas += cliente.registros.length;
      let registros = cliente.registros;
      registros.map(llamada => {
        let llamadaCliente = {
          rut: cliente.rut,
          nombre: cliente.nombre,
          fecha: llamada.idllamada,
          segundos: llamada.segundos
        }
        if (llamada.segundos > 0) {
          sumaContestadas += 1;
          llamadasContestadas.push(llamadaCliente)
        } else {
          sumaNoContestadas += 1;
          llamadasNoContestadas.push(llamadaCliente)

        }
      })


    });

    this.historial.totalLlamadas = sumaLlamadas;
    this.historial.sumaContestadas = sumaContestadas;
    this.historial.sumaNoContestadas = sumaNoContestadas;
    this.historial.porcentajeContestadas = sumaContestadas / sumaLlamadas;
    this.historial.porcentajeNoContestadas = sumaNoContestadas / sumaLlamadas;
    this.historial.llamadasContestadas = llamadasContestadas.sort((a, b) => {
      if (a.fecha < b.fecha) {
        return 1;
      }
      if (a.fecha > b.fecha) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });
    this.historial.llamadasNoContestadas = llamadasNoContestadas.sort((a, b) => {
      if (a.fecha < b.fecha) {
        return 1;
      }
      if (a.fecha > b.fecha) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });

  }

  /**
    *metodo que se ejecuta despues de iniciar la vista
    * @memberof DetalleClientePage
    */
   ngAfterViewInit() {

    this.slideLlamada.lockSwipes(true);

  }

  /**
   *permite cambiar la vista de slider
   * @param {*} event
   * @param {*} slide
   * @memberof DetalleClientePage
   */
  cambioPagina(event, slide) {

    this.slideLlamada.lockSwipes(false);

    this.slideLlamada.slideTo(event.detail.value.index);
    this.slideLlamada.lockSwipes(true);

  }
}
