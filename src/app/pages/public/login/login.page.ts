import { Component, OnInit } from '@angular/core';
import { AutentificacionService } from 'src/app/services/autentificacion/autentificacion.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage{

  /**
   * Formulario de login
   * @type {FormGroup}
   * @memberof LoginPage
   */
  loginForm: FormGroup;

  /**
   *Creates an instance of LoginPage.
   * @param {AutentificacionService} authService
   * @param {FormBuilder} formBuilder
   * @param {Router} router
   * @memberof LoginPage
   */
  constructor(private authService: AutentificacionService, private formBuilder: FormBuilder, private router: Router) {
    this.loginForm = this.formBuilder.group({
      usuario: [''],
      contrasenia: [''],
    });
  }

  /**
   * Permite obtener login para iniciar sesion en la aplicacion
   * @memberof LoginPage
   */
  logForm() {
    let { usuario, contrasenia } = this.loginForm.value
  
    let usuarioLogin = {
      strUsuario: usuario,
      strPassword: contrasenia
    }
    this.authService.login(usuarioLogin);

  }
  /* login() {
    this.authService.login();
  } */
}
