import { Component, OnInit } from '@angular/core';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { Cliente, Documentocobro } from 'src/app/interfaces/cliente';
import { ClienteService } from 'src/app/services/cliente/cliente.service';

@Component({
  selector: 'app-cobros-pendietes',
  templateUrl: './cobros-pendietes.component.html',
  styleUrls: ['./cobros-pendietes.component.scss'],
})
export class CobrosPendietesComponent implements OnInit {
  datos: any;
  lstPendientes: Documentocobro[];
  cliente: Cliente;
  obtener: boolean
  constructor(private dataLocal: DataLocalService,private clienteService: ClienteService) {
    this.datos = {};
    this.lstPendientes = [];
    this.obtener = true;

  }

  async ngOnInit() {
    this.datos = await this.dataLocal.getItem('cliente');
  
    this.cliente =await this.clienteService.obtenerCliente(this.datos);
    this.lstPendientes = await this.cliente.documento_cobros;
    console.log('pendientesssssssssss',this.lstPendientes);
    this.obtener = false;
  }

}
