import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { Evento } from 'src/app/interfaces/evento';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { ModalController, NavController, Events } from '@ionic/angular';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { DetalleClienteComponent } from '../modals/detalle-cliente/detalle-cliente.component';
import { PluginService } from 'src/app/services/plugin/plugin.service';
import { ComentarioLlamadaComponent } from '../modals/comentario-llamada/comentario-llamada.component';

@Component({
  selector: 'app-listado-clientes',
  templateUrl: './listado-clientes.component.html',
  styleUrls: ['./listado-clientes.component.scss'],
})
export class ListadoClientesComponent implements OnInit,OnChanges {
  @Input() lstClientes;
  @Input()recordsFound;
  @Input() textoBuscar: string;
  evento: Evento;
  obtener:boolean
  constructor(
    private navCtrl: NavController,
    private callNumber: CallNumber,
    private clienteService : ClienteService,
    private pluginService : PluginService,
    public modalController: ModalController,
    public events: Events,
    private dataLocal: DataLocalService) {
    this.obtener=true
   }

  ngOnInit() {}


  ngOnChanges(){
 
    this.lstClientes = this.lstClientes.sort((a, b) => {
      if (a.rankin > b.rankin) {
        return 1;
      }
      if (a.rankin < b.rankin) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });;
  }
  llamarCliente(cliente){
    console.log('aaaa')
    this.pluginService.llamarCliente(cliente);
    
    this.detalleCliente(cliente)
    /* this.evento = new Evento();
    this.evento.tipoEvento = 'LLAMADA';
    this.evento.rutCliente = cliente.rut;
    this.evento.estado = "LLAMADA";
    this.clienteService.generarEvento(this.evento);
    //this.clienteService.guardarLlamada(cliente);
    this.callNumber.callNumber(cliente.celular, true)
          .then(res => console.log('Launched dialer!', res))
          .catch(err => console.log('Error launching dialer', err));  */
  }

  async detalleCliente(cliente){
    console.log(this.events)

    await this.dataLocal.setItem('cliente',cliente);
    
    this.navCtrl.navigateForward(['/detalle-cliente'/* , JSON.stringify(cliente) */]);
   
  }

  async abrirModal(cliente){
  
    const modal = await this.modalController.create({
      component: ComentarioLlamadaComponent,
      componentProps: {
        cliente : cliente
      }
    });
    return await modal.present();
  
}
}
