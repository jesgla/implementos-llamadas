import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Evento } from 'src/app/interfaces/evento';
import { ModalController } from '@ionic/angular';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';

@Component({
  selector: 'app-comentario-llamada',
  templateUrl: './comentario-llamada.component.html',
  styleUrls: ['./comentario-llamada.component.scss'],
})
export class ComentarioLlamadaComponent implements OnInit {

   /**
   *objeto cliente
   * @type {Cliente}
   * @memberof ComentarioPage
   */
  @Input() cliente: any;

  /**
   *formulario cliente
   * @type {FormGroup}
   * @memberof ComentarioPage
   */
  comentarioForm: FormGroup;

  /**
   *objeto evento
   * @type {Evento}
   * @memberof ComentarioPage
   */
  registrarComentario: Evento;

  /**
   *arreglo de comentarios
   * @memberof ComentarioPage
   */
  autoComentario = [
    { comentario: 'Oportunidad de venta' },
    { comentario: 'Celular no corresponde a cliente' },
    { comentario: 'Cliente no esta disponible' },
    { comentario: 'Cliente no necesita nada' },
    { comentario: 'Cliente compro en la competencia' },
  ];

  /**
   *id de referencia check-in
   * @type {*}
   * @memberof ComentarioPage
   */
  idReferencia: any;

  /**
   *Creates an instance of ComentarioPage.
   * @param {ModalController} modalController
   * @param {FormBuilder} formBuilder
   * @param {EventoService} eventoService
   * @param {ToastController} toastController
   * @param {DataLocalService} dataLocal
   * @memberof ComentarioPage
   */
  constructor(public modalController: ModalController,
    private formBuilder: FormBuilder,
    private clienteService: ClienteService,
    private dataLocal: DataLocalService) {
    this.comentarioForm = this.formBuilder.group({
      comentario: ['', Validators.required]
    });

    this.registrarComentario = new Evento();
  }

  /**
   *metodo que se ejecuta al iniciar el componente
   * @memberof ComentarioPage
   */
  async ngOnInit() {
    
  }

  /**
   *permite cerrar el modal
   * @memberof ComentarioPage
   */
  cerrarModal() {

    this.modalController.dismiss();
  }

  /**
   *permite obtener datos del formulario
   * @memberof ComentarioPage
   */
  async logForm() {
    this.registrarComentario.tipoEvento = 'COMENTARIO';
    this.registrarComentario.fechaCreacion = new Date().toString();
    this.registrarComentario.fechaEjecucion = new Date().toString();
    this.registrarComentario.fechaProgramada = "";
    this.registrarComentario.observaciones = this.comentarioForm.get('comentario').value;
    this.registrarComentario.rutCliente = this.cliente.rut;
    this.registrarComentario.estado = "";

    let respuesta = await this.clienteService.generarEvento(this.registrarComentario);
    if (respuesta[0].resultado) {
      await this.dataLocal.presentToast('Comentario regitrado exitosamente')
      this.cerrarModal();
    } else {
      await this.dataLocal.presentToast('No se logro registrar comentario')
    }


  }
  /**
   *permite agregar calendario
   * @param {*} texto
   * @memberof ComentarioPage
   */
  addComentario(texto) {
    let comentario = this.comentarioForm.get('comentario').value;
    comentario += ` ${texto.comentario}`;
    this.comentarioForm.controls['comentario'].setValue(comentario)

  }

}
