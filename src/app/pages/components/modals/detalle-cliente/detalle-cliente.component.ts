import { Component, OnInit, Input, AfterViewInit, ViewChild } from '@angular/core';
import { ModalController, IonSlides } from '@ionic/angular';
import { ClienteService } from 'src/app/services/cliente/cliente.service';

@Component({
  selector: 'app-detalle-cliente',
  templateUrl: './detalle-cliente.component.html',
  styleUrls: ['./detalle-cliente.component.scss'],
})
export class DetalleClienteComponent implements OnInit,AfterViewInit {
  @Input() cliente: any;
  datos: any;
  
  /**
   *elemento slider
   * @type {IonSlides}
   * @memberof DetalleClientePage
   */
  @ViewChild(IonSlides, { static: false }) theSlides: IonSlides;

  /**
   *color de elemento
   * @type {*}
   * @memberof DetalleClientePage
   */
  color: any;

  /**
   *arreglo de paginas de slider
   * @memberof DetalleClientePage
   */
  paginas = [
    { icon: 'person', index: 0 },
    { icon: 'clipboard', index: 1 },
  ];
  constructor(
    public modalController: ModalController,
    private clienteService: ClienteService
  ) { 
    this.cliente = {};
  }

  async ngOnInit() {
 
    this.datos =await this.clienteService.obtenerCliente(this.cliente);
    this.cliente.correo =this.datos.correos && this.datos.correos[0]?this.datos.correos[0].correo:'no registrado';
 

  }
  cerrarModal(){
    this.modalController.dismiss();
  }
 /**
   *metodo que se ejecuta despues de iniciar la vista
   * @memberof DetalleClientePage
   */
  ngAfterViewInit() {
    //console.log('aca',this.theSlides.lockSwipes(true))
    //this.theSlides.lockSwipes(true);

  }

  /**
   *permite cambiar la vista de slider
   * @param {*} event
   * @param {*} slide
   * @memberof DetalleClientePage
   */
  cambioPagina(event, slide) {
    console.log('entre',slide)
    //this.theSlides.lockSwipes(false);
    console.log('entre',this.theSlides)
    this.theSlides.slideTo(event.detail.value.index);
    //this.theSlides.lockSwipes(true);

  }
}
