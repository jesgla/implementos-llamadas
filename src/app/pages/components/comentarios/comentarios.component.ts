import { Component, OnInit } from '@angular/core';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { ClienteService } from 'src/app/services/cliente/cliente.service';

@Component({
  selector: 'app-comentarios',
  templateUrl: './comentarios.component.html',
  styleUrls: ['./comentarios.component.scss'],
})
export class ComentariosComponent implements OnInit {
  datos: any;
  lstComentarios: any[];
  cliente: any;
  obtener: boolean;
  constructor(private dataLocal: DataLocalService, private clienteService: ClienteService) {
    this.datos = {};
    this.lstComentarios = [];
    this.obtener = true
  }

  async ngOnInit() {
    this.cliente = await this.dataLocal.getItem('cliente');

    this.lstComentarios = await this.clienteService.obtenerEventos(this.cliente);
    
    this.obtener = false;
    console.log('termine servicio', this.obtener)
  }
}
