import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-listado-llamadas',
  templateUrl: './listado-llamadas.component.html',
  styleUrls: ['./listado-llamadas.component.scss'],
})
export class ListadoLlamadasComponent implements OnInit {
  @Input() llamadas: any[];
  constructor() { }

  ngOnInit() { }

}
