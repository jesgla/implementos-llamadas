import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.scss'],
})
export class PedidosComponent implements OnInit {

  /**
   *listado de pedidos
   * @type {Pedido[]}
   * @memberof PedidosComponent
   */
  listadoPedido: any[];


  /**
   *objeto vendedor
   * @type {Vendedor}
   * @memberof PedidosComponent
   */
  vendedor: any;

  /**
   *estado de informacion
   * @type {boolean}
   * @memberof PedidosComponent
   */
  obtener: boolean;
  cliente: any;

  /**
   *Creates an instance of PedidosComponent.
   * @param {ClienteService} clienteService
   * @param {DataLocalService} dataLocal
   * @memberof PedidosComponent
   */
  constructor(private clienteService: ClienteService, private dataLocal: DataLocalService, ) {
    this.obtener = true
  }

  /**
   *metodo que se ejecuta al iniciar el componente
   * @memberof PedidosComponent
   */
  async ngOnInit() {
    await this.obtenerUsuario();
    await this.obtenerPedidos();
  }

  /**
   *permite obtener el listado de los pedidos de un cliente
   * @returns listado de cliente
   * @memberof PedidosComponent
   */
  async obtenerPedidos() {
    this.listadoPedido = await this.clienteService.obtenerPedidos(this.cliente);
    this.obtener = false;
    console.log('pedidooooo',this.listadoPedido )
    return this.listadoPedido;
  }

  /**
   *permite obtener usuario
   * @memberof PedidosComponent
   */
  async obtenerUsuario() {
   
    this.cliente = await this.dataLocal.getItem('cliente')

  }

}
