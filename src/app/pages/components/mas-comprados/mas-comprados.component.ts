import { Component, OnInit } from '@angular/core';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { ClienteService } from 'src/app/services/cliente/cliente.service';

@Component({
  selector: 'app-mas-comprados',
  templateUrl: './mas-comprados.component.html',
  styleUrls: ['./mas-comprados.component.scss'],
})
export class MasCompradosComponent implements OnInit {
  cliente: any;
  listadoArticulo: any;
  constructor(
    private dataLocal: DataLocalService,
    private clienteService: ClienteService
  ) { 
    this.listadoArticulo = [];
  }

  async ngOnInit() {

    
    this.cliente = await this.dataLocal.getItem('cliente');

    this.listadoArticulo = await this.clienteService.obtenerMasVendidos(this.cliente);
    console.log('this.listadoArticulo',this.listadoArticulo)
  }

}
