import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasCompradosComponent } from './mas-comprados.component';

describe('MasCompradosComponent', () => {
  let component: MasCompradosComponent;
  let fixture: ComponentFixture<MasCompradosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasCompradosComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasCompradosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
