import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListadoClientesComponent } from './listado-clientes/listado-clientes.component';
import { IonicModule } from '@ionic/angular';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { DetalleClienteComponent } from './modals/detalle-cliente/detalle-cliente.component';
import { ComentarioLlamadaComponent } from './modals/comentario-llamada/comentario-llamada.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DatosClienteComponent } from './datos-cliente/datos-cliente.component';
import { PedidosComponent } from './pedidos/pedidos.component';
import { CobrosPendietesComponent } from './cobros-pendietes/cobros-pendietes.component';
import { ComentariosComponent } from './comentarios/comentarios.component';
import { ListadoLlamadasComponent } from './listado-llamadas/listado-llamadas.component';
import { HeaderComponent } from './header/header.component';
import { MasCompradosComponent } from './mas-comprados/mas-comprados.component';
import { Ng2Rut } from 'ng2-rut';



@NgModule({
  entryComponents:[DetalleClienteComponent,ComentarioLlamadaComponent],
  declarations: [
    ListadoClientesComponent,
    DetalleClienteComponent,
    ComentarioLlamadaComponent,
    DatosClienteComponent,
    PedidosComponent,
    CobrosPendietesComponent,
    ComentariosComponent,
    ListadoLlamadasComponent,
    HeaderComponent,
    MasCompradosComponent
  ],
  exports:[
    ListadoClientesComponent,
    DetalleClienteComponent,
    ComentarioLlamadaComponent,
    DatosClienteComponent,
    PedidosComponent,
    CobrosPendietesComponent,
    ComentariosComponent,
    ListadoLlamadasComponent,
    MasCompradosComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    PipesModule,
    ReactiveFormsModule,
    FormsModule,
    Ng2Rut
    
  ]
})
export class ClienteComponentsModule { }
