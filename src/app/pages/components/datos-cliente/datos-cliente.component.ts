import { Component, OnInit } from '@angular/core';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { ModalController, AlertController } from '@ionic/angular';
import { ComentarioLlamadaComponent } from '../modals/comentario-llamada/comentario-llamada.component';
import { Cliente } from 'src/app/interfaces/cliente';
import { PluginService } from 'src/app/services/plugin/plugin.service';

@Component({
  selector: 'app-datos-cliente',
  templateUrl: './datos-cliente.component.html',
  styleUrls: ['./datos-cliente.component.scss'],
})
export class DatosClienteComponent implements OnInit {

  cliente: any;
  datos: Cliente;


  /**
   *color de elemento
   * @type {*}
   * @memberof DetalleClientePage
   */
  color: any;

  /**
   *arreglo de paginas de slider
   * @memberof DetalleClientePage
   */
  paginas = [
    { icon: 'person', index: 0 },
    { icon: 'clipboard', index: 1 },
    { icon: 'call', index: 2 },
  ];
  listadoArticulo: [];
  constructor(
    public alertController: AlertController,
    private pluginService: PluginService,
    public modalController: ModalController,
    private dataLocal: DataLocalService,
    private clienteService: ClienteService
  ) {
    this.cliente = {};

    this.listadoArticulo = []
  }

  async ngOnInit() {
    this.cliente = await this.dataLocal.getItem('cliente');
    
    this.datos = await this.clienteService.obtenerCliente(this.cliente);
    this.cliente.correo = this.datos.correos && this.datos.correos[0] ? this.datos.correos[0].correo : 'no registrado';
  
  



  }
  async abrirModal() {

    const modal = await this.modalController.create({
      component: ComentarioLlamadaComponent,
      componentProps: {
        cliente: this.cliente
      }
    });
    return await modal.present();

  }
  llamarCliente(cliente) {
 
    this.pluginService.llamarCliente(cliente);

  }
  editarCampo(campo, cliente) {

  }

  async accionEditarCelular(campo, cliente) {
    const alert = await this.alertController.create({
      subHeader: 'Ingrese nuevo telefono',
      message: `numero actual ${cliente.celular}`,
      inputs: [
        {
          name: campo,
          type: 'number',
          label: '+5699873455'
        },
        
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Cambiar',
          handler: (data) => {
            console.log('Confirm Ok',data);

            this.presentAlertConfirm(cliente,data.celular,cliente.celular,campo)
          }
        }
      ]
    });

    await alert.present();
  }
  async accionEditarCorreo(campo,cliente){
    let correo = cliente.correos && cliente.correos[0]?cliente.correos[0].correo:'';
    console.log(cliente.correos)
    let mensaje=correo=!''?`correo actual <span style="font-size: 14px !important;"> ${correo} </span>`:'';
    let alert = await this.alertController.create({
      subHeader: 'Ingrese nuevo correo',
      message: `${mensaje}`,
      inputs: [
        {
          name: campo,
          type: 'text',
          label: 'micorreo@correo.cl'
        },
        
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Cambiar',
          handler: (data) => {
            console.log('Confirm Ok',data);
            let validateObj = this.validateEmail(data.correo);
            console.log('Confirm Ok',validateObj);
            if (!validateObj.isValid) {
              console.log('entre');
              this.dataLocal.presentToast('Correo invalido')
             

              //alert.message['<br>Your validation message'];
                return false;
            } else {
                //make HTTP call
              console.log('enwwweewtre');

                this.presentAlertConfirm(cliente,data.correo,correo,campo)
            }
            
          }
        }
      ]
    });

    await alert.present();
  }
  async presentAlertConfirm(cliente, dataNueva,valorAnterior,tipo) {
    let mensaje = tipo =='correo'?'¿Desea cambiar el correo?':'¿Desea cambiar el celular?';
    const alert = await this.alertController.create({
      header: 'Confirmar',
      message:mensaje,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: async () => {
            let parametros = {
              rut:cliente.rut,
              campo: tipo =='correo'?dataNueva:'+'+dataNueva,
              tipoCampo: tipo ,
              valorAnterior: valorAnterior
        
            }
    
            await this.clienteService.actualizarCampo(parametros);
            
            if(tipo =='celular'){
              this.cliente.celular= '+'+dataNueva
            }
            this.datos = await this.clienteService.obtenerCliente(this.cliente);
            this.cliente.correo = this.datos.correos && this.datos.correos[0] ? this.datos.correos[0].correo : 'no registrado';
          }
        }
      ]
    });

    await alert.present();
  }

  validateEmail(correo) {
    if( /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(correo) ){
      return {
        isValid: true,
        message: ''
      };
    } else {
       return {
          isValid: false,
          message: 'Email address is required'
       }
    }
}
}
