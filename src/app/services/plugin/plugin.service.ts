import { Injectable } from '@angular/core';
import { Evento } from 'src/app/interfaces/evento';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { ClienteService } from '../cliente/cliente.service';

@Injectable({
  providedIn: 'root'
})
export class PluginService {
  evento: Evento;

  constructor(
    private callNumber: CallNumber,
    private clienteService : ClienteService
  ) { }

  llamarCliente(cliente){
    this.evento = new Evento();
    this.evento.tipoEvento = 'LLAMADA';
    this.evento.rutCliente = cliente.rut;
    this.evento.estado = "LLAMADA";
    this.clienteService.generarEvento(this.evento);
    //this.clienteService.guardarLlamada(cliente);
    this.callNumber.callNumber(cliente.celular, true)
          .then(res => console.log('Launched dialer!', res))
          .catch(err => console.log('Error launching dialer', err)); 
  }
}
