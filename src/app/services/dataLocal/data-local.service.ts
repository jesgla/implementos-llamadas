import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {
 

  constructor(public toastController: ToastController,private storage: Storage) { }

  async presentToast( message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 1500
    });
    toast.present();
  }
  async setItem(clave: string, cliente: any) {
    await this.storage.set(clave,cliente);
  }
  /**
   *Permite obtener un elemento de la local storage
   * @param {string} elemento
   * @returns
   * @memberof DataLocalService
   */
  async getItem(elemento: string){
    return await this.storage.get(elemento);
  }
}
