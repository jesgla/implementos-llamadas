import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';

import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';
import { DataLocalService } from '../dataLocal/data-local.service';
import { HttpClient } from '@angular/common/http';
const TOKEN_KEY = 'auth-token';

@Injectable({
  providedIn: 'root'
})
export class AutentificacionService {


  authenticationState = new BehaviorSubject(false);

  constructor(
    private storage: Storage, 
    private plt: Platform, 
    private httpClient: HttpClient, 
    private dataLocal: DataLocalService) {
    this.plt.ready().then(() => {
      this.checkToken();
    });
  }

  checkToken() {
    this.storage.get(TOKEN_KEY).then(res => {
      if (res) {
        this.authenticationState.next(true);
      }
    })
  }

  async login(usuario) {
    let consulta = null;
    const url = `https://b2b-api.implementos.cl/api/movil/loginUsuario`;
    consulta = await this.httpClient.post<any>(url, usuario).toPromise();
    console.log('consulta', consulta);
    if (consulta.length > 0) {
      await this.storage.remove(TOKEN_KEY);
      return this.storage.set(TOKEN_KEY, JSON.stringify(consulta[0])).then(() => {
        this.authenticationState.next(true);
      });
    } else {
      this.dataLocal.presentToast('Usuario o contraseña incorrecta');
    }
 
  }

  logout() {
    return this.storage.remove(TOKEN_KEY).then(() => {
      this.authenticationState.next(false);
    });
  }

  isAuthenticated() {
    return this.authenticationState.value;
  }

}
