import { Injectable, ɵConsole } from '@angular/core';
import { DataLocalService } from '../dataLocal/data-local.service';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { Evento } from 'src/app/interfaces/evento';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {



  vendedor: any;
  crearEvento: Evento;
  /**
    *header para peticiones post
    * @memberof EventoService
    */
  header = new HttpHeaders(
    {
      "Accept": 'application/json',
      'Content-Type': 'application/json',
      'response-Type': 'json'
    }

  );
  constructor(
    private dataLocal: DataLocalService,
    private httpClient: HttpClient
  ) { }

  async obtenerUsuario() {
    let usuario = await this.dataLocal.getItem('auth-token');
  
    this.vendedor = JSON.parse(usuario);
    console.log('usuario',this.vendedor)
    return this.vendedor;
  }

  async obtenerClientes() {
    let consulta = null
    await this.obtenerUsuario();
    let body = {
      id: this.vendedor.codEmpleado
    }

    //const url = `https://b2b-api.implementos.cl/api/carro/clientesvendedor`;
    const url = `https://b2b-api.implementos.cl/api/movil/clientesvendedor`;
    consulta = await this.httpClient.post<any>(url, body).toPromise();
 
    //let datos= await this.obtenerDatosClientes(consulta.data);
    return consulta.data;
  }
  async obtenerDatosClientes(data: any) {
    for (let index = 0; index < data.length; index++) {
      const cliente = data[index];
      let objetoCli = await this.obtenerCliente(cliente);

      cliente.correo = objetoCli.correos && objetoCli.correos[0] ? objetoCli.correos[0].correo : 'no registrado';
      cliente.creditoDisponible = objetoCli.credito - objetoCli.creditoUtilizado;
    }
    return data
  }

  async obtenerCliente(cliente) {
    let consulta = null;
    let parametros = {
      rut: cliente.rut.replace('.', '').replace('.', ''),
      token: cliente.rut

    }

    //const url = `https://b2b-api.implementos.cl/api/cliente/GetDatosCliente`;
    const url = `https://b2b-api.implementos.cl/api/movil/getDatosCliente`;

    consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();

    return consulta.data[0];
  }

  async obtenerEventos(cliente) {
    let consulta = null;
    await this.obtenerUsuario();
    let parametros = {
      strTipoEvento:'COMENTARIO',
      rutVendedor : this.vendedor.rut,
      rutCliente: cliente.rut.replace('.', '').replace('.', ''),

    }
    //onst url = `http://localhost:1900/api/movil/obtenerEventosCliente`;

    const url = `https://b2b-api.implementos.cl/api/movil/obtenerEventosCliente`;

    consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();
    console.log('obtenerEventos', consulta)
    return consulta;
  }
  
  async obtenerMasVendidos(cliente) {
    let consulta = null;
    let parametros = {
      rut: cliente.rut.replace('.', '').replace('.', ''),

    }
    const url = `https://b2b-api.implementos.cl/api/movil/masvendidos?rut=${cliente.rut.replace('.', '').replace('.', '')}`;
    //const url = `http://localhost:1900/api/movil/masvendidos?rut=${cliente.rut.replace('.', '').replace('.', '')}`;

    //const url = `https://b2b-api.implementos.cl/api/carro/masvendidos?rut=${cliente.rut.replace('.', '').replace('.', '')}`;

    consulta = await this.httpClient.get(url).toPromise();
    console.log('acaaa', consulta)
    return consulta.data;
  }

  async guardarLlamada(cliente) {
    let consulta = null
    await this.obtenerUsuario();
    let params = {
      rut: cliente.rut,
      celular: cliente.celular,
      id: this.vendedor.codEmpleado,
      segundos: cliente.segundos,
      idllamada: cliente.idllamada
    }
    const url = `https://b2b-api.implementos.cl/api/carro/registrollamado`;

    consulta = await this.httpClient.post<any>(url, params).toPromise();
    console.log('llamandaaa ', consulta);
    return consulta.data;
  }

  /**
  *Permite generar un evento de cliente
  * @param {*} objeto
  * @returns
  * @memberof EventoService
  */
  async generarEvento(objeto) {
    await this.obtenerUsuario();
    let consulta = null;
    let url = `https://b2b-api.implementos.cl/api/movil/agendarVisita`;
    let parametros = this.setearObjeto(objeto);

    consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();
    console.log('consutla', consulta)
    return consulta;
  }

  async obtenerPedidos(cliente) {
    let consulta = null;
    await this.obtenerUsuario();
    let parametros = {
      rutVendedor: this.vendedor.rut,
      rutCliente: cliente.rut

    }

    const url = `https://b2b-api.implementos.cl/api/movil/listadoPedidos`;
    consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();

    return consulta;
  }

  async actualizarCampo(datos) {
    let consulta = null;

    let parametros = datos
    console.log('dsd', parametros)
    const url = `https://b2b-api.implementos.cl/api/movil/actualizarDatos`;
    consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();
    console.log('acaaa',consulta)
    this.dataLocal.presentToast('se actualizo con exito el registro')
    return consulta;
  }

  async obtenerOfertas(parametros){
    let consulta = null;

    console.log('dsd', parametros)
    const url = `https://b2b-api.implementos.cl/api/movil/actualizarDatos`;
    consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();
    console.log('acaaa',consulta)
    this.dataLocal.presentToast('se actualizo con exito el registro')
    return consulta;
  }
  /**
   *Permite ordenar la fecha para almacenar
   * @param {Date} fechaCompleta
   * @returns
   * @memberof EventoService
   */
  ordenarFecha(fechaCompleta: Date) {

    fechaCompleta = new Date(fechaCompleta);
    console.log('fecha 78', fechaCompleta)

    let dia = fechaCompleta.getDate();
    let mes = fechaCompleta.getMonth() + 1;
    let anio = fechaCompleta.getFullYear();
    let hora = fechaCompleta.getHours();
    let min = fechaCompleta.getMinutes();
    mes = mes > 12 ? 1 : mes;
    let diaSrt = dia < 10 ? '0' + dia : dia;
    let mesStr = mes < 10 ? '0' + mes : mes;
    let horaStr = hora < 10 ? '0' + hora : hora;
    let minStr = min < 10 ? '0' + min : min;

    let formatoFecha = `${anio}${mesStr}${diaSrt}${horaStr}${minStr}`
    console.log('salida 90', formatoFecha)
    return formatoFecha

  }

  /**
   *Permite llenar objeto de evento
   * @param {*} objeto
   * @returns
   * @memberof EventoService
   */
  setearObjeto(objeto) {
    this.crearEvento = new Evento();
    this.crearEvento.idEvento = objeto.idEvento ? objeto.idEvento : '',
      this.crearEvento.refVisita = objeto.refVisita ? objeto.refVisita : '',
      this.crearEvento.tipoEvento = objeto.tipoEvento ? objeto.tipoEvento : '',
      this.crearEvento.fechaCreacion = objeto.fechaCreacion ? this.ordenarFecha(objeto.fechaCreacion) : '',
      this.crearEvento.fechaEjecucion = objeto.fechaEjecucion ? this.ordenarFecha(objeto.fechaEjecucion) : '',
      this.crearEvento.fechaProgramada = objeto.fechaProgramada ? this.ordenarFecha(objeto.fechaProgramada) : '',
      this.crearEvento.observaciones = objeto.observaciones ? objeto.observaciones : '',
      this.crearEvento.rutEmisor = this.vendedor.rut,
      this.crearEvento.rutVendedor = this.vendedor.rut,
      this.crearEvento.rutCliente = objeto.rutCliente ? objeto.rutCliente : '',
      this.crearEvento.estado = objeto.estado ? objeto.estado : '',
      this.crearEvento.direccion = objeto.direccion ? objeto.direccion : '',
      this.crearEvento.latitud = objeto.latitud ? objeto.latitud : '',
      this.crearEvento.longitud = objeto.longitud ? objeto.longitud : '',
      this.crearEvento.referenciaEvento = objeto.referenciaEvento ? objeto.referenciaEvento : ''
    console.log('evento 90', this.crearEvento)
    return this.crearEvento;
  }

}
