import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

import { AutentificacionService } from '../services/autentificacion/autentificacion.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard  implements CanActivate {
 
  constructor(public auth: AutentificacionService) {}
 
  canActivate(): boolean {
    return this.auth.isAuthenticated();
  }
}